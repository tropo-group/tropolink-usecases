{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Define location ensemble with R to run trajectory ensembles in tropolink\n",
    "\n",
    "#### Author: Samuel Soubeyrand (INRAE, BioSP, 84914 Avignon, France; samuel.soubeyrand@inrae.fr)\n",
    "\n",
    "#### R code for generating ensemble locations around a set of starting/arrival locations. Locations are saved in a text file with a format compatible with tropolink input (https://tropolink.fr). Trajectory ensembles are generated to account for uncertainty in starting/arrival locations. For a given initial location, each member of the location ensemble is calculated by translating starting/arrival latitude, longitude and altitude by a fixed value in (-u,0,+u), u depending on whether one considers the latitude, the longitude or the altitude. This results in 27 members of the location ensembles for each starting/arrival location. R code designed with R version 4.2.2 (2022-10-31).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### Load required R packages\n",
    "library(maps) ## map()\n",
    "library(sp) ## SpatialPoints(), SpatialPointsDataFrame()\n",
    "library(OpenStreetMap) ## openmap(), openproj()\n",
    "\n",
    "#### Load a function for the construction of the location ensemble \n",
    "\n",
    "location.ensemble=function(locations,u,proj=\"+proj=longlat +ellps=WGS84\"){\n",
    "\t## locations is a data frame with five columns: code, name, lat, long and alt\n",
    "\t## u is a list with three elements: lat, long and alt\n",
    "\t## In locations and u, specify long and lat in the projection system specified by proj\n",
    "\t## and specify alt in meters\n",
    "\t## Default proj is WGS84 (i.e., latitudes and longitudes defined in decimal degrees)\n",
    "\t## In locations:\n",
    "\t## code: codes of starting/arrival locations (without space)\n",
    "\t## name: names of starting/arrival locations (without space)\n",
    "\t## lat: latitudes of starting/arrival locations\n",
    "\t## long: longitudes of starting/arrival locations\n",
    "\t## alt: altitudes of starting/arrival locations, in meters\n",
    "\t## In u:\n",
    "\t## lat: lag in latitude for building the location ensembles\n",
    "\t## long: lag in longitude for building the location ensembles\n",
    "\t## alt: lag in altitude for building the location ensembles, in meters\n",
    "\t\n",
    "\t## Generating location ensemble around each starting/arrival location\n",
    "\tensemble=expand.grid(c(-1,0,1)*u$lat,c(-1,0,1)*u$long,c(-1,0,1)*u$alt)\n",
    "\tcoord=code=name=NULL\n",
    "\tfor(i in 1:nrow(locations)){\n",
    "\t\tcoord=rbind(coord,t(as.numeric(locations[i,3:5])+t(ensemble)))\n",
    "\t\tcode=c(code,paste(locations$code[i],1:27,sep=\"_\"))\n",
    "\t\tname=c(name,paste(locations$name[i],1:27,sep=\"_\"))\n",
    "\t}\n",
    "\t## set minimum altitude above ground level at 0\n",
    "\tcoord[,3]=pmax(0,coord[,3])\n",
    "\tgrid0=SpatialPoints(coords=cbind(coord[,2],coord[,1]),proj4string=CRS(proj))\n",
    "\t\n",
    "\t## Project the grid in the WGS84 system\n",
    "\tgrid.WGS84=spTransform(grid0,CRS=CRS(\"+proj=longlat +ellps=WGS84\"))\n",
    "\tcoordx.WGS84=grid.WGS84@coords[,1]\n",
    "\tcoordy.WGS84=grid.WGS84@coords[,2]\n",
    "\t\t\t\n",
    "\t## Return the coordinates of the grid points in the WGS84 system as a SpatialPoints object\n",
    "\tout=SpatialPointsDataFrame(coords=grid.WGS84@coords,proj4string=CRS(\"+proj=longlat +ellps=WGS84\"),\n",
    "\t\tdata=data.frame(code,name,lat=grid.WGS84@coords[,2],long=grid.WGS84@coords[,1],alt=coord[,3]))\n",
    "\treturn(out)\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Boundaries of the land domain in the WGS84 projection system\n",
    "Blat=c(43,45)\n",
    "Blong=c(4,6)\n",
    "\n",
    "## Map background\n",
    "map0=openmap(c(Blat[2],Blong[1])+c(1,-1)*2,c(Blat[1],Blong[2])+c(-1,1)*2,type=\"osm\")\n",
    "map0=openproj(map0, projection = \"+proj=longlat +ellps=WGS84\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Example 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Define starting/arrival locations in WGS84 projection system\n",
    "locations1=data.frame(code=c(\"CLE\",\"CHA\",\"MAR\"),name=c(\"Clermont_Ferrand\",\"Chambéry\",\"Marseille\"),\n",
    "\tlat=c(45.78,45.57,43.28),long=c(3.11,5.92,5.37),alt=rep(750,3))\n",
    "\t\n",
    "## Generate location ensemble\n",
    "grid1=location.ensemble(locations1,list(lat=0.25,long=0.25,alt=250),proj=\"+proj=longlat +ellps=WGS84\")\n",
    "\n",
    "## Map location ensemble (note that points with same latitudes and longitudes but different altitudes\n",
    "## are drawn together; hence, one sees only 9 points instead of 27 for each starting/arrival location)\n",
    "plot(map0)\n",
    "points(grid1@coords,pch=19,cex=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Save coordinates in a format consistent with tropolink input\n",
    "## See format constraints below (note here that one has to provide a code and a name for\n",
    "##  each grid point as well as the height above ground level for the arrival/starting \n",
    "##  of the air mass trajectories, for each grid point)\n",
    "head(grid1@data)\n",
    "write.table(grid1@data,\"location_ensemble1_for_tropolink.txt\",col.names=FALSE,row.names=FALSE,quote=FALSE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Example 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Define starting/arrival locations in Lambert-93 projection system\n",
    "locations2.L93=data.frame(code=c(\"CLE\",\"CHA\",\"MAR\"),name=c(\"Clermont_Ferrand\",\"Chambéry\",\"Marseille\"),\n",
    "\tlat=c(6520049,6500933,6245202),long=c(708547.3,927701.4,892461.5),alt=rep(750,3))\n",
    "\n",
    "## Generate location ensemble in WGS84 projection system\n",
    "grid2=location.ensemble(locations2.L93,list(lat=25000,long=25000,alt=250),proj=\"+init=epsg:2154\")\n",
    "\n",
    "## Map location ensemble (note that points with same latitudes and longitudes but different altitudes\n",
    "## are drawn together; hence, one sees only 9 points instead of 27 for each starting/arrival location)\n",
    "plot(map0)\n",
    "points(grid2@coords,pch=19,cex=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Save coordinates in a format consistent with tropolink input\n",
    "## See format constraints below (note here that one has to provide a code and a name for\n",
    "##  each grid point as well as the height above ground level for the arrival/starting \n",
    "##  of the air mass trajectories, for each grid point)\n",
    "head(grid2@data)\n",
    "write.table(grid2@data,\"location_ensemble2_for_tropolink.txt\",col.names=FALSE,row.names=FALSE,quote=FALSE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Format constraints for specifying starting/arrival locations of forward/backward trajectories:\n",
    "\n",
    "Fields should be separated by at least one space.\n",
    "\n",
    "Station names must not contain any space character.\n",
    "\n",
    "Coordinates should be in degrees in dot-decimal format (WGS84; EPSG4326). Latitudes b/n -90 and 90 degrees (negative values for Southern hemisphere). Longitudes b/n -180 and 180 degrees (negative values for Western hemisphere wrt Greench meridian).\n",
    "\n",
    "Norm: Code Name Latitude[deg] Longitude[deg] Arrival/starting_height_above_ground_level[m]\n",
    "\n",
    "Example: STA StationName 50.28 2.78 1000"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "4.2.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
