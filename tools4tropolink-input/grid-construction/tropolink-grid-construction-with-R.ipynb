{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Build a regular spatial grid with R overlapping a specific geographic region\n",
    "\n",
    "#### Author: Samuel Soubeyrand (INRAE, BioSP, 84914 Avignon, France; samuel.soubeyrand@inrae.fr)\n",
    "\n",
    "#### R code for constructing a regular grid limited to a land domain (up to a tolerance distance). Locations of grid points are saved in a text file with a format compatible with tropolink input (https://tropolink.fr). R code designed with R version 4.2.2 (2022-10-31).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### Load required R packages and functions\n",
    "library(maps) ## map()\n",
    "library(OpenStreetMap) ## openmap(), openproj()\n",
    "library(sp)\n",
    "\n",
    "source(\"point.in.SpatialPolygons.prevR.R\") ## function from the archived prevR package\n",
    "source(\"map2spatialPolygons.maptools.R\") ## function from the archived maptools package\n",
    "\n",
    "\n",
    "#### Load a function for the grid construction\n",
    "make.grid=function(Blong,Blat,resol,tolerance.bw,proj=\"+proj=longlat +ellps=WGS84\"){\n",
    "\t## Specify Blong, Blat, resol and tolerance.bw in the projection system specified by proj\n",
    "\t## Default proj is WGS84 (i.e., latitudes and longitudes defined in decimal degrees)\n",
    "\t## Blong: extreme longitudes of the bounding box\n",
    "\t## Blat: extreme latitudes of the bounding box\n",
    "\t## resol: resolution of the grid, i.e. regular spatial lag between neighboor grid points\n",
    "\t##     in both longitude and latitude\n",
    "\t## tolerance.bw: tolerance in both longitude and latitude for avoiding to exclude grid points\n",
    "\t##     outside the land domain but near the shoreline; set tolerance.bw=0 to exclude any grid\n",
    "\t##     point outside the land domain\n",
    "\t\n",
    "\t## Define the boundaries of the land domain\n",
    "\tworld=map(\"world\",fill=TRUE,plot=FALSE)\n",
    "\tIDs=sapply(strsplit(world$names, \":\"), function(x) x[1])\n",
    "\tworld=map2SpatialPolygons(world, IDs=IDs, proj4string=CRS(\"+proj=longlat +ellps=WGS84\"))\n",
    "\n",
    "\t## Define the raw grid, given that whatever Blong, Blat and resol, (Blong[1],Blat[1]) is one\n",
    "\t##     of the points of the raw grid\n",
    "\tseqx=seq(Blong[1],Blong[2],resol)\n",
    "\tseqy=seq(Blat[1],Blat[2],resol)\n",
    "\tnx=length(seqx)\n",
    "\tny=length(seqy)\n",
    "\tncell=nx*ny\n",
    "\tcat(paste(\"Number of cells:\",ncell,\"\\n\"))\n",
    "\tcoordx=matrix(seqx,nx,ny)\n",
    "\tcoordy=t(matrix(seqy,ny,nx))\n",
    "\tgrid0=SpatialPoints(coords=cbind(as.vector(coordx),as.vector(coordy)),proj4string=CRS(proj))\n",
    "\t\n",
    "\t## Project the raw grid in the WGS84 system\n",
    "\tgrid.WGS84=spTransform(grid0,CRS=CRS(\"+proj=longlat +ellps=WGS84\"))\n",
    "\tcoordx.WGS84=matrix(grid.WGS84@coords[,1],nx,ny)\n",
    "\tcoordy.WGS84=matrix(grid.WGS84@coords[,2],nx,ny)\n",
    "\t\n",
    "\t## Identify the grid points within the land domain, up to an eventual tolerance\n",
    "\tdomain=matrix(point.in.SpatialPolygons(coordx.WGS84,coordy.WGS84, world),nx,ny)\n",
    "\tif(tolerance.bw>0){\n",
    "\t\ttranslation=t(tolerance.bw*sapply(seq(0,2*pi,l=9)[-1],function(u) c(cos(u),sin(u))))\n",
    "\t\tfor(i in 1:nrow(translation)){\n",
    "\t\t\tcoordx.tol=matrix(coordx+translation[i,1],nx,ny)\n",
    "\t\t\tcoordy.tol=matrix(coordy+translation[i,2],nx,ny)\n",
    "\t\t\tgrid.tol=SpatialPoints(coords=cbind(as.vector(coordx.tol),as.vector(coordy.tol)),\n",
    "\t\t\t\tproj4string=CRS(proj))\n",
    "\t\t\tgrid.tol.WGS84=spTransform(grid.tol,CRS=CRS(\"+proj=longlat +ellps=WGS84\"))\n",
    "\t\t\tcoordx.tol.WGS84=matrix(grid.tol.WGS84@coords[,1],nx,ny)\n",
    "\t\t\tcoordy.tol.WGS84=matrix(grid.tol.WGS84@coords[,2],nx,ny)\n",
    "\t\t\tdomain=domain+matrix(point.in.SpatialPolygons(coordx.tol.WGS84,coordy.tol.WGS84, \n",
    "\t\t\t\tworld),nx,ny)\n",
    "\t\t}\n",
    "\t}\n",
    "\tdomain=(domain>0)+0\n",
    "\tcoords.WGS84=cbind(longitude=as.numeric(coordx.WGS84)[as.numeric(domain)==1],\n",
    "\t\tlatitude=as.numeric(coordy.WGS84)[as.numeric(domain)==1])\n",
    "\t\t\n",
    "\t## Return the coordinates of the grid points in the WGS84 system as a SpatialPoints object\n",
    "\tout=SpatialPoints(coords=coords.WGS84,proj4string=CRS(\"+proj=longlat +ellps=WGS84\"))\n",
    "\treturn(out)\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Boundaries of the land domain in the WGS84 projection system (to draw country limits)\n",
    "world=map(\"world\",fill=TRUE,plot=FALSE)\n",
    "IDs=sapply(strsplit(world$names, \":\"), function(x) x[1])\n",
    "world=map2SpatialPolygons(world, IDs=IDs, proj4string=CRS(\"+proj=longlat +ellps=WGS84\"))\n",
    "\n",
    "## Define the bounding box (~ Southeastern France)\n",
    "Blat=c(41,46)\n",
    "Blong=c(3,10)\n",
    "\n",
    "## Map background including a buffer zone with respect to the bounding box\n",
    "map0=openmap(c(Blat[2],Blong[1])+c(1,-1)*2,c(Blat[1],Blong[2])+c(-1,1)*2,type=\"osm\")\n",
    "map0=openproj(map0, projection = \"+proj=longlat +ellps=WGS84\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Build two grids with make.grid() in the WGS84 projection system, with two different tolerance\n",
    "par(mfrow=c(1,1))\n",
    "grid=make.grid(Blong,Blat,0.5,0)\n",
    "grid.tol=make.grid(Blong,Blat,0.5,0.1)\n",
    "plot(map0)\n",
    "mtext(\"Grid points in WGS84 without (black) or with (red) tolerance\")\n",
    "plot(world,add=TRUE)\n",
    "points(grid@coords,pch=19,cex=0.5)\n",
    "points(grid.tol@coords,col=2,lwd=2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Build a grid with make.grid() in the Lambert 93 projection system\n",
    "proj1=\"+init=epsg:2154\"\n",
    "## Project the map backgroundproj1\n",
    "map1=openproj(map0, projection = proj1)\n",
    "## Project the map background without the buffer zone\n",
    "map0temp=openmap(c(Blat[2],Blong[1]),c(Blat[1],Blong[2]),type=\"osm\")\n",
    "map0temp=openproj(map0temp, projection = \"+proj=longlat +ellps=WGS84\")\n",
    "map1temp=openproj(map0temp, projection = proj1)\n",
    "## Apply make.grid\n",
    "grid1.WGS84=make.grid(Blong=c(map1temp$bbox$p1[1],map1temp$bbox$p2[1]),\n",
    "\tBlat=c(map1temp$bbox$p2[2],map1temp$bbox$p1[2]),50*10^3,10*10^3,proj1)\n",
    "## Display the grid in the WGS84 system\n",
    "plot(map0)\n",
    "#text((map1$bbox$p1[1]+map1$bbox$p2[1])/2,map0$bbox$p1[2],\"Grid points in WGS84\",pos=4)\n",
    "mtext(\"Grid points in WGS84\")\n",
    "plot(world,add=TRUE)\n",
    "points(grid1.WGS84@coords,pch=19)\n",
    "## Display the grid in the Lambert 93 system\n",
    "world1=spTransform(world,CRS=CRS(proj1))\n",
    "grid1=spTransform(grid1.WGS84,CRS=CRS(proj1))\n",
    "plot(map1)\n",
    "#text((map1$bbox$p1[1]+map1$bbox$p2[1])/2,map1$bbox$p1[2],\"Grid points in Lambert 93\",pos=4)\n",
    "mtext(\"Same grid points in Lambert 93\")\n",
    "plot(world1,add=TRUE)\n",
    "points(grid1@coords,pch=19)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Save coordinates in a format consistent with tropolink input\n",
    "## See format constraints below (note here that one has to provide a code and a name for\n",
    "##     each grid point as well as the height above ground level for the arrival/starting \n",
    "##  \t   of the air mass trajectories, for each grid point)\n",
    "ID=paste(\"Sector\",1:length(grid1.WGS84),sep=\"\")\n",
    "loc=data.frame(code=ID,name=ID,lat=grid1.WGS84@coords[,2],long=grid1.WGS84@coords[,1],\n",
    "\taltitude=rep(1000,length(ID)))\n",
    "head(loc)\n",
    "write.table(loc,\"locations_for_tropolink.txt\",col.names=FALSE,row.names=FALSE,quote=FALSE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Format constraints for specifying starting/arrival locations of forward/backward trajectories:\n",
    "\n",
    "Fields should be separated by at least one space.\n",
    "\n",
    "Station names must not contain any space character.\n",
    "\n",
    "Coordinates should be in degrees in dot-decimal format (WGS84; EPSG4326). Latitudes b/n -90 and 90 degrees (negative values for Southern hemisphere). Longitudes b/n -180 and 180 degrees (negative values for Western hemisphere wrt Greench meridian).\n",
    "\n",
    "Norm: Code Name Latitude[deg] Longitude[deg] Arrival/starting_height_above_ground_level[m]\n",
    "\n",
    "Example: STA StationName 50.28 2.78 1000"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "4.4.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
