# tropolink-usecases
## Finality
Examples and tutorials are under construction in a wiki (mixing English and French) within the gitlab repository of the**tropolink**webapp: [https://forgemia.inra.fr/tropo-group/tropolink/-/wikis/home](https://forgemia.inra.fr/tropo-group/tropolink/-/wikis/home).

We hope that members of the**tropolink**community will share their codes for analyzing**tropolink**output with Python, R and any other relevant software. These codes may be referenced in the [Example page of the wiki](https://forgemia.inra.fr/tropo-group/tropolink/-/wikis/Exemples) (under tropolink-api project).


## Getting started With Gitlab

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

### Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://forgemia.inra.fr/tropo-group/tropolink-usecases.git
git branch -M main
git push -uf origin main
```

### Integrate with your tools

- [ ] [Set up project integrations](https://forgemia.inra.fr/tropo-group/tropolink-usecases/-/settings/integrations)

### Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

### Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Tropolink Project recalling

### Support
Support is contacted via the mailing list <tropolink-users@groupes.renater.fr> This list is accessible only to users of the tropolink web application.
It will be first of all a channel of information concerning the evolution technological aspect of the tropolink Web application (maintenance, upgrade...) but may also serve as a channel for sharing information on the use of tropolink (new use, new article...) and channel to do report difficulties / questions related to tropolink.the latter messages will be filtered to avoid overloading the list and processed directly by the back office or forwarded to the entire list if their scope is general.

### Help and FAQ
https://tropolink.fr/help

### Contributing, credits and legal notice 
* Landing page : https://forgemia.inra.fr/tropo-group/tropolink/-/wikis/home 
* Credit and legal notice : https://tropolink.fr/credit

