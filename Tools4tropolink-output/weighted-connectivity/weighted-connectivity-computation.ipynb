{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Computation of weighted connectivity between geographic locations from air mass trajectories\n",
    "\n",
    "#### Author: Samuel Soubeyrand (INRAE, BioSP, 84914 Avignon, France; samuel.soubeyrand@inrae.fr)\n",
    "\n",
    "#### R code for computing weighted connectivity between geographic locations from air mass trajectories where the weights depend on variables computed by HYSPLIT, including meteorological variables characterizing the air mass. This is a modification of the \"Connectivity code\" (v1.1 (2022-12-13)) by Samuel SOUBEYRAND, Hervé RICHARD, Davide MARTINETTI and embedded in the tropolink web application (https://tropolink.fr/) for computing non-weighted connectivity. This modified R code was designed with R version 4.2.2 (2022-10-31)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Functions and input variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### Load required R packages\n",
    "library(sf) ## st_xxx()\n",
    "library(jsonlite) ## read.json()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### Load function for the computation of weighted connectivity\n",
    "\n",
    "## tdump.filenames: vector of tdump filenames including the directory path\n",
    "## locations.indices: indices for the subset of locations considered in the connectivity\n",
    "##   study within the list of locations in the trajectory study\n",
    "## coords: matrix of coordinates of nodes considered in the connectivity study\n",
    "##   (col1: long, col2: lat)\n",
    "## buffer.type: type of buffer, either buffer (Circular buffer) or geographic_buffer\n",
    "##   (Geographic buffer)\n",
    "## buffer.radius: radius of buffer, in km if Circular buffer, in degrees if Geographic buffer\n",
    "## studyname: string with the name of the study\n",
    "weighted.connectivity=function(tdump.filenames,locations.indices,coords,\n",
    "\tbuffer.type,buffer.radius,filter,studyname){\n",
    "    ## Calculate buffer contours\n",
    "\tsources=buffer.contour(coords,buffer.type,buffer.radius)\n",
    "\t## Initial value for the matrix of connectivity to be computed (sum of contacts)\n",
    "\tCONNECT=0\n",
    "\t## Loop over all dates considered in the connectivity study\n",
    "\tfor(k in 1:length(tdump.filenames)){ \n",
    "\t    ## Load trajectory data\n",
    "\t    TDraw=scan(tdump.filenames[k],quiet=TRUE,what=\"character\")\n",
    "\t    TD=matrix(as.numeric(TDraw[(which(TDraw==\"SUN_FLUX\")+1):length(TDraw)]),ncol=22,byrow=TRUE)\n",
    "\t    colnames(TD)=c(\"TRAJ_NB\",\"MET_GRID_NB\",\"YEAR\",\"MONTH\",\"DAY\",\"HOUR\",\"MINUTE\",\"FORECAST_HOUR\",\n",
    "\t\t\t\"AGE\",\"LATITUDE\",\"LONGITUDE\",\"ALTITUDE\",\"PRESSURE\",\"THETA\",\"AIR_TEMP\",\"RAINFALL\",\n",
    "\t\t\t\"MIXDEPTH\",\"RELHUMID\",\"SPCHUMID\",\"H2OMIXRA\",\"TERR_MSL\",\"SUN_FLUX\")\n",
    "\t\tTD=as.data.frame(TD)\n",
    "\t\t## Extract relevant trajectory subsets for every node considered in the connectivity study\n",
    "\t\ttraj=sapply(locations.indices,function(u){\n",
    "\t\t\t## Filter trajectory tables\n",
    "\t\t\tfiltering=(TD['TRAJ_NB']==u & sapply(TD['HOUR'],function(u) u%in%filter$values.hour) &\n",
    "\t\t\t\tabs(TD['AGE'])>=filter$range.abs.age[1] & abs(TD['AGE'])<=filter$range.abs.age[2])\n",
    "\t\t\tfor(i in 1:ncol(filter$ranges)){\n",
    "\t\t\t\tfiltering=filtering & TD[colnames(filter$ranges)[i]]>=filter$ranges[1,i] & \n",
    "\t\t\t\t\tTD[colnames(filter$ranges)[i]]<=filter$ranges[2,i]\n",
    "\t\t\t}\n",
    "\t\t\ttraj.coords=TD[filtering,c('LONGITUDE', 'LATITUDE','AGE')]\n",
    "            traj.coords=traj.coords[!is.na(traj.coords[,1]),]\n",
    "\t\t\t## Set 1 point outside the spatial domain for sub-trajectories without point\n",
    "\t\t\tif(nrow(traj.coords)==0){\n",
    "\t\t\t\ttraj.coords=cbind(1000,1000,0)\n",
    "\t\t\t}\n",
    "\t\t\t## Trajectory subsets stored as multilines\n",
    "\t\t\treturn(list(st_multilinestring(split.traj(traj.coords))))\n",
    "\t\t})\n",
    "\t\t## Trajectories stored as a sf geometry list in the WGS84 projection\n",
    "\t\ttraj=st_sfc(traj,crs=4326)\n",
    "\t\t## Compute the intersection between buffers and trajectories to cumpute the connectivity\n",
    "\t\t## matrix for 1 date\n",
    "\t\t## rows: target nodes (i.e. starting or arrival point)\n",
    "\t\t## columns: source polygons (buffers around distant target nodes)\n",
    "\t\t## row i, column j: 1 if trajectory starting/arriving at target point i went through buffer j\n",
    "\t\tconnect1=st_intersects(traj,sources, sparse=FALSE)+0\n",
    "\t\t## Increment connectivity\n",
    "\t\tCONNECT=CONNECT+connect1\t\n",
    "\t    ## Display information about the progression of the computation\n",
    "\t    if(length(tdump.filenames)<20 | k/20==round(k/20) | k==length(tdump.filenames)){\n",
    "\t        cat(\" Percent complete:\",round(k/length(tdump.filenames)*100,digits=1),\"\\n\")\n",
    "\t    }\n",
    "\t}\n",
    "\t## Save connectivity matrix as json and txt files\n",
    "\trownames(CONNECT)=paste(\"S/A-point\",rownames(coords),sep=\"_\")\n",
    "\tcolnames(CONNECT)=paste(\"Buffer\",rownames(coords),sep=\"_\")\n",
    "\toutput.C=NULL\n",
    "\toutput.C$name=studyname\n",
    "\toutput.C$connectivity=list(matrix=CONNECT,row_names=rownames(CONNECT),col_names=colnames(CONNECT))\n",
    "\toutput.C$readme=\"Value at (row i , column j): number of times that trajectories starting/arriving at point i went through buffer j\"\n",
    "\treturn(list(connectivity.matrix=CONNECT,connectivity.json=output.C))\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### Load function for calculating buffer contours and store them as a sf geometry list in the WGS84 projection\n",
    "\n",
    "## coords: matrix of coordinates of nodes considered in the connectivity study\n",
    "##   (col1: long, col2: lat)\n",
    "## buffer.type: type of buffer, either buffer (Circular buffer) or geographic_buffer\n",
    "##   (Geographic buffer)\n",
    "## buffer.radius: radius of buffer, in km if Circular buffer, in degrees if Geographic buffer\n",
    "buffer.contour=function(coords,buffer.type,buffer.radius){\n",
    "\t## Buffer contours for circular buffers\n",
    "\tif(buffer.type==\"buffer\"){\n",
    "\t\t## Standard circular buffer\n",
    "\t\tbuffer0=cbind(cos(seq(0,2*pi,l=100)),sin(seq(0,2*pi,l=100)))\n",
    "\t\tbuffer0=rbind(buffer0[-nrow(buffer0),],buffer0[1,])\n",
    "\t\tbuffer.set=apply(coords,1,function(u){\n",
    "\t\t\t## Buffer defined in a kilometric projection centered around the node location\n",
    "\t\t\tlocal_azimuthal_projection = paste(\"+proj=aeqd +R=6371000 +units=km +lat_0=\",u[2],\n",
    "\t\t\t\t\" +lon_0=\",u[1],sep=\"\")\n",
    "\t\t\tbufferCirc=buffer0*buffer.radius\n",
    "\t\t\tbufferCirc=st_sfc(st_polygon(list(bufferCirc)),crs=local_azimuthal_projection)\n",
    "\t\t\t## Buffer projected in WGS84 (latitude-longitude)\n",
    "\t\t\tbufferCirc.WGS84=st_transform(bufferCirc,4326)\n",
    "\t\t\treturn(bufferCirc.WGS84[[1]])\n",
    "\t\t})\t\n",
    "\t}\n",
    "\t## Buffer contours for geographic buffers\n",
    "\tif(buffer.type==\"geographic_buffer\"){\n",
    "\t\t## Standard geographic buffer\n",
    "\t\tbuffer0=rbind(c(-1,-1),c(-1,1),c(1,1),c(1,-1),c(-1,-1))\n",
    "\t\t## Buffer defined around the node locations\n",
    "\t\tbuffer.set=apply(coords,1,function(u) st_polygon(list(t(u+t(buffer0*buffer.radius)))))\n",
    "\t}\n",
    "\t## Buffers stored as a sf geometry list in the WGS84 projection\n",
    "\tbuffer.set=st_sfc(buffer.set,crs=4326) \n",
    "\treturn(buffer.set)\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### Load function for splitting a trajectory matrix x composed of several pieces of trajectories separated \n",
    "#### in time into several sub-trajectory matrices\n",
    "split.traj=function(x){\n",
    "\tindex=cbind(1,NA)\n",
    "\ti=2\n",
    "\twhile(i<=nrow(x)){\n",
    "\t\twhile(abs(x$AGE[i])==abs(x$AGE[i-1])+1 & i<=nrow(x)){\n",
    "\t\t\ti=i+1\t\t\n",
    "\t\t}\n",
    "\t\tindex[nrow(index),2]=i-1\n",
    "\t\tindex=rbind(index,c(i,NA))\n",
    "\t\ti=i+1\n",
    "\t}\n",
    "\tif(is.na(index[nrow(index),2])){ \n",
    "\t\tindex[nrow(index),2]=index[nrow(index),1]\n",
    "\t}\n",
    "\tif(index[nrow(index),1]>nrow(x)){ \n",
    "\t\tindex=rbind(index[-nrow(index),]) \n",
    "\t}\n",
    "\tout=NULL\n",
    "\tfor(j in 1:nrow(index)){\n",
    "\t\tsubtraj=as.matrix(rbind(x[index[j,1]:index[j,2],1:2]))\n",
    "\t\tif(nrow(subtraj)==1){ \n",
    "\t\t\t## add a 2nd very-close point to sub-trajectories with only 1 point to form a trajectory\n",
    "\t\t\t## with 2 points and allow to make the intersection between trajectories and polygons\n",
    "\t\t\tsubtraj=rbind(subtraj,subtraj+10^-6)\n",
    "\t\t}\n",
    "\t\tout=c(out,list(subtraj))\n",
    "\t}\n",
    "\treturn(out)\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### Load input and set useful variables\n",
    "\n",
    "## Specifications of the trajectory study. Arguments required: locations and dates\n",
    "input.T=read_json(\"tropolink_output_files/example-for-weighted-connectivity.json\")\n",
    "\n",
    "## Data frame of locations in the trajectory study\n",
    "locations.T=as.data.frame(lapply(input.T$locations,function(u) strsplit(u$value,\" \")[[1]]))\n",
    "names(locations.T)=locations.T[1,]\n",
    "\n",
    "## Data frame of locations considered in the connectivity study (select the sites (i.e. the\n",
    "##    columns) in locations.T that you want to keep in the connectivity)\n",
    "locations=locations.T\n",
    "\n",
    "## Indices for the subset of locations considered in the connectivity study within the list\n",
    "## of locations in the trajectory study\n",
    "locations.indices=sapply(names(locations),function(u) which(names(locations.T)==u))\n",
    "\n",
    "## Matrix of coordinates of nodes considered in the connectivity study\n",
    "coords=t(apply(locations,2,function(u) as.numeric(u[4:3])))\n",
    "colnames(coords)=c(\"long\",\"lat\")\n",
    "\n",
    "## Vector of dates in the trajectory study\n",
    "dates.T=as.character(lapply(input.T$dates, function(u) \n",
    "\tformat(as.Date(u[[1]]),format=\"%y-%m-%d\")))\n",
    "\t\n",
    "## Vector of dates at which connectivity is computed (select the dates in dates.T that you want\n",
    "##   to keep in the connectivity study)\n",
    "dates=dates.T\n",
    "\n",
    "## Directory (with \"/\" at the end) where the tdump files corresponding to the trajectory \n",
    "## study are located\n",
    "tdumpfiles.directory=\"tropolink_output_files/tdumps_example-for-weighted-connectivity/\"\n",
    "\n",
    "## Vector of filenames (including the path) of trajectory files (i.e. tdump files) \n",
    "tdump.filenames=paste(tdumpfiles.directory,\"tdump_\",dates,sep=\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Summary statistics about all the variables that can be filtered\n",
    "variable.filter=c(6,9,12:22)\n",
    "TD.alldates=NULL\n",
    "for(k in 1:length(tdump.filenames)){ \n",
    "    ## Load trajectory data\n",
    "    TDraw=scan(tdump.filenames[k],quiet=TRUE,what=\"character\")\n",
    "    TD=matrix(as.numeric(TDraw[(which(TDraw==\"SUN_FLUX\")+1):length(TDraw)]),ncol=22,byrow=TRUE)\n",
    "    colnames(TD)=c(\"TRAJ_NB\",\"MET_GRID_NB\",\"YEAR\",\"MONTH\",\"DAY\",\"HOUR\",\"MINUTE\",\"FORECAST_HOUR\",\n",
    "                   \"AGE\",\"LATITUDE\",\"LONGITUDE\",\"ALTITUDE\",\"PRESSURE\",\"THETA\",\"AIR_TEMP\",\"RAINFALL\",\n",
    "                   \"MIXDEPTH\",\"RELHUMID\",\"SPCHUMID\",\"H2OMIXRA\",\"TERR_MSL\",\"SUN_FLUX\")\n",
    "    TD.alldates=rbind(TD.alldates,as.data.frame(TD))\n",
    "}\n",
    "summary(TD.alldates[,variable.filter])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Example 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Example 1: all trajectory points contribute to connectivity\n",
    "\n",
    "## Select hours in the day (UTC) at which connectivity is computed\n",
    "VALUES.HOUR_1=unique(TD.alldates[,\"HOUR\"])\n",
    "## Set minimum and maximum number of hours apart from the starting/arrival hour used for \n",
    "##    defining the period within the trajectory duration over which connectivity is computed\n",
    "##    (absolute integer value between 0 and abs(input.T$runtime))\n",
    "## input.T$runtime gives the duration of trajectories in the trajectory file (in hours, \n",
    "##    absolute value)\n",
    "RANGE.ABS.AGE_1=range(abs(TD.alldates[,\"AGE\"]))\n",
    "## For each variable in (\"ALTITUDE\",\"PRESSURE\",\"THETA\",\"AIR_TEMP\",\"RAINFALL\",\n",
    "## \"MIXDEPTH\",\"RELHUMID\",\"SPCHUMID\",\"H2OMIXRA\",\"TERR_MSL\",\"SUN_FLUX\"), set minimum and maximum \n",
    "## values defining the interval over which connectivity is computed\n",
    "RANGES_1=as.data.frame(apply(TD.alldates[,c(\"ALTITUDE\",\"PRESSURE\",\"THETA\",\"AIR_TEMP\",\"RAINFALL\",\n",
    "\t\"MIXDEPTH\",\"RELHUMID\",\"SPCHUMID\",\"H2OMIXRA\",\"TERR_MSL\",\"SUN_FLUX\")],2,range))\n",
    "\n",
    "## Compute and display connectivity matrix\n",
    "connect1=weighted.connectivity(tdump.filenames,locations.indices,coords,\n",
    "\tbuffer.type=\"buffer\",buffer.radius=150,\n",
    "\tfilter=list(values.hour=VALUES.HOUR_1,range.abs.age=RANGE.ABS.AGE_1,ranges=RANGES_1),\n",
    "\tstudyname=\"example-for-weighted-connectivity_1\")\n",
    "\t\n",
    "image(connect1$connectivity.matrix,asp=1,axes=FALSE,main=\"Connectivity matrix 1\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Example 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Example 2: trajectory points contribute to connectivity only if the altitude of the\n",
    "##     air mass is lower than or equal to 1000m above ground level\n",
    "\n",
    "VALUES.HOUR_2=VALUES.HOUR_1\n",
    "RANGE.ABS.AGE_2=RANGE.ABS.AGE_1\n",
    "RANGES_2=RANGES_1\n",
    "RANGES_2$ALTITUDE=c(0,1000)\n",
    "\n",
    "## Compute and display connectivity matrix\n",
    "connect2=weighted.connectivity(tdump.filenames,locations.indices,coords,\n",
    "\tbuffer.type=\"buffer\",buffer.radius=150,\n",
    "\tfilter=list(values.hour=VALUES.HOUR_2,range.abs.age=RANGE.ABS.AGE_2,ranges=RANGES_2),\n",
    "\tstudyname=\"example-for-weighted-connectivity_2\")\n",
    "\n",
    "image(connect2$connectivity.matrix,asp=1,axes=FALSE,main=\"Connectivity matrix 2\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Example 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Example 3: trajectory points contribute to connectivity only if the altitude of the\n",
    "##     air mass is lower than or equal to 1000m above ground level and if the time of the day\n",
    "##     is between 8pm and 6am UTC\n",
    "\n",
    "VALUES.HOUR_3=c(0:6,20:23)\n",
    "RANGE.ABS.AGE_3=RANGE.ABS.AGE_1\n",
    "RANGES_3=RANGES_1\n",
    "RANGES_3$ALTITUDE=c(0,1000)\n",
    "\n",
    "## Compute and display connectivity matrix\n",
    "connect3=weighted.connectivity(tdump.filenames,locations.indices,coords,\n",
    "\tbuffer.type=\"buffer\",buffer.radius=150,\n",
    "\tfilter=list(values.hour=VALUES.HOUR_3,range.abs.age=RANGE.ABS.AGE_3,ranges=RANGES_3),\n",
    "\tstudyname=\"example-for-weighted-connectivity_3\")\n",
    "\n",
    "image(connect3$connectivity.matrix,asp=1,axes=FALSE,main=\"Connectivity matrix 3\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###### Example 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Example 4: trajectory points contribute to connectivity only if the aboslute age of the\n",
    "##     air mass is lower than or equalt to 12h\n",
    "\n",
    "VALUES.HOUR_4=VALUES.HOUR_1\n",
    "RANGE.ABS.AGE_4=c(0,12)\n",
    "RANGES_4=RANGES_1\n",
    "\n",
    "## Compute and display connectivity matrix\n",
    "connect4=weighted.connectivity(tdump.filenames,locations.indices,coords,\n",
    "\tbuffer.type=\"buffer\",buffer.radius=150,\n",
    "\tfilter=list(values.hour=VALUES.HOUR_4,range.abs.age=RANGE.ABS.AGE_4,ranges=RANGES_4),\n",
    "\tstudyname=\"example-for-weighted-connectivity_4\")\n",
    "\n",
    "image(connect4$connectivity.matrix,asp=1,axes=FALSE,main=\"Connectivity matrix 4\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Histograms and pair plots of the different connectivity measures\n",
    "\n",
    "panel.hist <- function(x, ...){\n",
    "    usr <- par(\"usr\")\n",
    "    par(usr = c(usr[1:2], 0, 1.5) )\n",
    "    h <- hist(x, plot = FALSE,breaks=50)\n",
    "    breaks <- h$breaks; nB <- length(breaks)\n",
    "    y <- h$counts; y <- y/max(y)\n",
    "    rect(breaks[-nB], 0, breaks[-1], y, col = \"red\", ...)\n",
    "}\n",
    "pairs(cbind(as.vector(connect1$connectivity.matrix),as.vector(connect2$connectivity.matrix),\n",
    "\tas.vector(connect3$connectivity.matrix),as.vector(connect4$connectivity.matrix)),\n",
    "\tdiag.panel=panel.hist,upper.panel=NULL,panel=function(x,y){panel.smooth(x,y);abline(0,1)},\n",
    "\tlabels=paste(\"Connectivity\",1:4))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Save connectivity in json and txt formats \n",
    "\n",
    "write_json(connect2$connectivity.json,\"weighted_connect_2.json\")\n",
    "write.table(connect2$connectivity.matrix,\"weighted_connect_2.txt\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "4.4.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
